FROM python:3.10.6-alpine3.16

# create the api user
#RUN addgroup --system api && adduser --system --group api

# https://docs.python.org/3/using/cmdline.html#envvar-PYTHONDONTWRITEBYTECODE
# Prevents Python from writing .pyc files to disk
ENV PYTHONDONTWRITEBYTECODE 1

# ensures that the python output is sent straight to terminal (e.g. your container log)
# without being first buffered and that you can see the output of your application (e.g. django logs)
# in real time. Equivalent to python -u: https://docs.python.org/3/using/cmdline.html#cmdoption-u
ENV PYTHONUNBUFFERED 1

WORKDIR /service
COPY ./requirements.txt /service/requirements.txt
RUN pip install --no-cache-dir --upgrade -r /service/requirements.txt
COPY ./app /service/app
COPY ./run_service.sh /service
CMD ["sh", "run_service.sh"]

# FROM python:3.8-slim
# RUN useradd -ms /bin/bash rtqueue

# WORKDIR /home/rtqueue
# COPY requirements.txt requirements.txt
# COPY module/dist/rtqueue-0.1-py3-none-any.whl rtqueue-0.1-py3-none-any.whl
# RUN pip install --force-reinstall -U rtqueue-0.1-py3-none-any.whl
# RUN pip install -r requirements.txt
# RUN apt-get update
# RUN apt-get -y install curl

# COPY api.py ./

# RUN chown -R rtqueue:rtqueue ./
# USER rtqueue

# CMD uvicorn api:app --host 0.0.0.0 --port 5057