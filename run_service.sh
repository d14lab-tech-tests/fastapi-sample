#!/bin/sh

curr_path=$(pwd)
export PYTHONPATH="${PYTHONPATH}:$curr_path/app"

PORT=8088
if [ ! -z ${SERVICE_PORT+x} ]; then
    PORT=$SERVICE_PORT
fi

uvicorn app.main:app --proxy-headers --host 0.0.0.0 --port $PORT
