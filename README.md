# FastAPI sample service 



## Getting started

```
$ python3 -m venv venv
$ source venv/bin/activate
$ pip install -U pip
$ pip install -r requirements.txt
```

If you add more dependencies to the project don't forget to add them to the `requirements.txt` file:

```
pip3 freeze > requirements.txt
```

Run the service:

```
uvicorn api:app --host 0.0.0.0 --port 7654
```
## Badges

## Installation

## Usage

