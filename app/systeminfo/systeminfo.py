from pydantic import BaseModel
import platform
from datetime import datetime

class SystemInfo(BaseModel):
  date_time: str
  system: str
  node_name: str
  release: str
  version: str
  machine: str
  processor: str

def get_system_info():
  my_system = platform.uname()

  system_info = SystemInfo(
    date_time=datetime.now().strftime('%d/%m/%Y %H:%M:%S'),
    system=my_system.system,
    node_name=my_system.node,
    release=my_system.release,
    version=my_system.version,
    machine=my_system.machine,
    processor=my_system.processor
  )

  return system_info

if __name__ == '__main__':
  print(get_system_info())
