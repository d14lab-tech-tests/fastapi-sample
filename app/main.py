from fastapi import FastAPI
from starlette.responses import RedirectResponse
from systeminfo.systeminfo import get_system_info

app = FastAPI()

@app.get('/')
def root():
  return RedirectResponse(url='/info')


@app.get('/info', status_code=200)
def get_info():
  """Retrieve info.

  Retrieve basic info about the system where the api service is running.
  """

  system_info = get_system_info()

  return system_info
